$("document").ready(function () {
  var initHeader = function () {
    var header = $("#shopify-section-new-header");

    var scrollPos = 200;
    window.addEventListener("scroll", function () {
      if (document.body.getBoundingClientRect().top < scrollPos) {
        // Scrolling up
        header.addClass("transparent");
      } else {
        // Scrolling down
        header.removeClass("transparent");
      }

      scrollPos = document.body.getBoundingClientRect().top;
    });
  };

  var handleCartOverlay = function () {
    var overlay = $("#shopify-section-cart-overlay");
    var hidden = "hidden";

    $(".close-cart-offer").on("click", function (e) {
      e.preventDefault();
      overlay.addClass(hidden);
    });
  };

  var handleProductQuanity = function () {
    var decrease = document.getElementsByClassName("product-quanity-decrease")[0];
    var increase = document.getElementsByClassName("product-quanity-increase")[0];
    var quanity = document.getElementsByClassName("product-quantity-input")[0];

    if (decrease) {
      decrease.addEventListener("click", function () {
        if (quanity.value > 1) {
          quanity.value = parseInt(quanity.value) - 1;
        }
      });
    }

    if (increase) {
      increase.addEventListener("click", function () {
        quanity.value = parseInt(quanity.value) + 1;
      });
    }
  };

  var initMobileHeader = function (e) {
    var trigger = $(".menu-toggle");
    var menu = $(".lsHeader-mobile-navigation-wrapper");
    var controls = $(".lsHeader-moibile-controls");
    var activeClass = "is-active";

    trigger.click(function (e) {
      trigger.toggleClass(activeClass);
      menu.toggleClass(activeClass);
      controls.toggleClass(activeClass);
    });
  };

  var handleSearchToggle = function () {
    var searchToggle = $(".lsHeader-search-toggle");
    var searchInput = $(".lsHeader-search-form");

    searchToggle.click(function (e) {
      e.preventDefault();
      searchInput.toggleClass("visible");
    });
  };

  var initInstagram = function () {
    $(window).on("load", function () {
      $.instagramFeed({
        username: "longshotdrink",
        container: "#instagram-feed",
        display_profile: false,
        display_biography: false,
        display_gallery: true,
        styling: false,
        items: 6,
      });
    });
  };

  var reviewsSlider = function () {
    $(".reviews-slider").slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: true,
      pauseOnHover: true,
      prevArrow:
        '<svg class="slick-carousel-button slick-carousel-button-prev" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path></svg>',
      nextArrow:
        '<svg class="slick-carousel-button slick-carousel-button-next" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg>',
      responsive: [
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  };

  var equationSlider = function () {
    $(".equation-carousel").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false,
      pauseOnHover: true,
    });
  };

  var siteOverlay = function () {
    var COOKIE_NAME = 'site-overlay-dismissed';
    var section = $('#shopify-section-site-overlay');
    var close = $('.site-overlay-close-button, .dismisOffer')

    if (!Cookies.get(COOKIE_NAME)) {
      section.addClass('visible')
    }

    close.on('click', function () {
      section.removeClass('visible');

      Cookies.set(COOKIE_NAME, true)
    })

  }

  initMobileHeader();
  // initHeader();
  handleCartOverlay();
  handleProductQuanity();
  handleSearchToggle();
  initInstagram();
  reviewsSlider();
  equationSlider();
  siteOverlay()
});
