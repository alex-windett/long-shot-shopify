# Long Shot Drinks Shopify Theme

The theme is addapted from the Startup theme - [https://themes.shopify.com/themes/startup/styles/home](https://themes.shopify.com/themes/startup/styles/home)

## Developemnt

Use [theme kit](https://shopify.github.io/themekit/) with help to develop new sections.

Make sure that your have set up a private Shopify app to allow for the connection, see [here](https://shopify.github.io/themekit/#get-api-access).

```bash
brew install themekit

theme get -p=[your-password] -s=[you-store.myshopify.com] -t=[your-theme-id]
```
